#include "shader.h"

omiShader::omiShader()
{
}

GLuint omiShader::_loadShader(const string &source, GLenum type)
{
    GLuint shader = glCreateShader(type);
    const char *str = source.c_str();
    glShaderSource(shader, 1, &str, 0);
    glCompileShader(shader);
    return _checkCompilation(shader) ? shader : 0;
}

GLuint omiShader::_link()
{
    GLuint prog = glCreateProgram();
    glAttachShader(prog, m_vert);
    glAttachShader(prog, m_frag);
    glBindAttribLocation(prog, ATT_TANGENTS, "tangent");
    glBindAttribLocation(prog, ATT_BONE_ID, "bone_id");
    glBindAttribLocation(prog, ATT_BONE_WEIGHT, "bone_weight");
    glLinkProgram(prog);
    if(!_checkLinking(prog)) return 0;
    
//    glValidateProgram(prog);
//    if(!_checkValidation(prog)) return 0;
    
    return prog;
}

void omiShader::_destroy()
{
    glDetachShader(m_prog, m_vert);
    glDetachShader(m_prog, m_frag);
    glDeleteProgram(m_prog);
    m_locations.clear();
    m_loaded = false;
}

bool omiShader::_checkCompilation(GLuint shader)
{
    GLint r;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &r);
    if(!r)
    {
        GLchar msg[1024];
        glGetShaderInfoLog(shader, sizeof(msg), 0, msg);
        printf("Compiling shader\nfailed: %s\n", msg);
        return 0;
    }
    return 1;
}

bool omiShader::_checkLinking(GLuint prog)
{
    GLint r;
    glGetProgramiv(prog, GL_LINK_STATUS, &r);
    if(!r)
    {
        GLchar msg[1024];
        glGetProgramInfoLog(prog, sizeof(msg), 0, msg);
        printf("Linking shaders failed: %s\n", msg);
        return 0;
    }
    return 1;
}

bool omiShader::_checkValidation(GLuint prog)
{
    GLint r;
    glGetProgramiv(prog, GL_VALIDATE_STATUS, &r);
    if(!r)
    {
        GLchar msg[1024];
        glGetProgramInfoLog(prog, sizeof(msg), 0, msg);
        printf("%s\n", msg);
        return 0;
    }
    return 1;
}

bool omiShader::load(const string &vertex, const string &fragment)
{
    m_loaded = false;
    if(!(m_vert = _loadShader(vertex, GL_VERTEX_SHADER))) return 0;
    if(!(m_frag = _loadShader(fragment, GL_FRAGMENT_SHADER))) return 0;
    if(!(m_prog = _link()))
    {
        _destroy();
        printf("Error in shader %s/%s\n", vertex.c_str(), fragment.c_str());
        return 0;
    }
    m_loaded = 1;
    return 1;
}

void omiShader::unload()
{
    _destroy();
}

void omiShader::use()
{
    glUseProgram(m_prog);
}

bool omiShader::isLoaded()
{
    return m_loaded;
}

GLuint omiShader::getUniformLocation(const string &name)
{
    GLuint loc = m_locations[name];
    if(loc == 0)
    {
        loc = glGetUniformLocation(m_prog, name.c_str());
        m_locations[name] = loc;
    }
    return loc;
}
