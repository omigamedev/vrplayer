#version 120
uniform sampler2D tex0;
uniform sampler2D tex1;
uniform ivec4 vp;
varying vec2 texcoord;
varying vec3 norm;
varying vec3 pos;
uniform vec3 light;
varying vec2 screen;
varying float depth;
varying float cam_att;

void main()
{
    vec2 poissonDisk[4] = vec2[](
                                 vec2( -0.94201624, -0.39906216 ),
                                 vec2( 0.94558609, -0.76890725 ),
                                 vec2( -0.094184101, -0.92938870 ),
                                 vec2( 0.34495938, 0.29387760 )
                                 );
    vec3 N = normalize(norm);
    vec3 L = normalize(light - pos);
    vec4 color = texture2D(tex0, texcoord);
    vec2 tshad = vec2((screen.x*0.5+0.5), screen.y*0.5+0.5);
    float diffuse = max(dot(N,L), 0.0);
    float visibility = 1.0;
    
//    if((tshad.x < 0.99 && tshad.x > 0.01) && (tshad.y < 0.99 && tshad.y > 0.01))
//    {
//        vec4 frontDepth = texture2D(tex1, tshad);
//        for (int i=0;i<4;i++)
//        {
//            if (texture2D(tex1, tshad + poissonDisk[i]/700.0 ).z  <  depth - 0.01 )
//            {
//                visibility-=0.2;
//            }
//        }
//    }
    
    gl_FragColor = color * diffuse * visibility * cam_att;
}
