#version 120
uniform mat4 proj;
uniform mat4 modelview;
uniform mat3 normal_matrix;

uniform sampler2D diffuse_tex;
uniform sampler2D normal_tex;
uniform sampler2D specular_tex;
uniform sampler2DShadow shadow_tex;

uniform vec2 normal_scale;
uniform vec2 specular_scale;
uniform vec2 diffuse_scale;

uniform vec3 light_pos;
uniform vec4 ambient_color;
uniform vec4 specular_color;
uniform vec4 diffuse_color;
uniform float shininess;

uniform bool has_specular_map;
uniform bool has_normal_map;
uniform bool has_diffuse_map;
uniform bool has_shadow_map;

uniform bool enable_shading;

varying vec4 lv;
varying vec3 l;
varying vec3 e;
varying vec3 v;
varying vec3 n;

uniform bool has_bones;
varying vec4 bone_color;

void main()
{
    vec2 poissonDisk[4] = vec2[](
         vec2( -0.94201624, -0.39906216 ),
         vec2( 0.94558609, -0.76890725 ),
         vec2( -0.094184101, -0.92938870 ),
         vec2( 0.34495938, 0.29387760 )
         );
    vec4 diffuse = diffuse_color;
    vec4 specular = vec4(0);
    vec3 bump = vec3(0);
    
    if(has_diffuse_map)
    {
        diffuse = texture2D(diffuse_tex, gl_TexCoord[0].st * diffuse_scale);
    }
    
    if(enable_shading)
    {
        bump = vec3(0,0,1);
        if(has_normal_map)
        {
            bump = (2.0 * texture2D(normal_tex, gl_TexCoord[0].st * normal_scale).rgb - 1.0);
        }
        
        vec3 N = normalize(bump);
        vec3 L = normalize(l);
        vec3 E = normalize(e);
        vec3 R = normalize(reflect(-L, N));
        
        specular = diffuse;
        if(has_specular_map)
        {
            specular = texture2D(specular_tex, gl_TexCoord[0].st * specular_scale);
        }
        
        specular = clamp(specular * pow(max(dot(R,E),0),shininess),0,1);
        diffuse = clamp(diffuse * max(dot(N,L),0),0,1);
    }
    
    float visibility = 1.0;
    float occlusion = 1.0;
    vec4 lp = lv / lv.w;
    if(has_shadow_map)
    {
        vec2 tshad = lp.xy;
        if((tshad.x < 0.97 && tshad.x > 0.03) && (tshad.y < 0.97 && tshad.y > 0.03))
        {
            //visibility = shadow2DProj(shadow_tex, lv).r;
            for(int i = 0; i < 4; i++)
            {
                if(shadow2DProj(shadow_tex, lv + vec4(poissonDisk[i]/30.0,0,0)).r == 0)
                {
                    visibility -= 0.2;
                    occlusion = 0;
                }
            }
        }
    }
    
    float att = 1.0;//clamp(1000.0 / pow(length(light_pos - v),2),0,1);
    gl_FragColor = (ambient_color*diffuse + diffuse + specular * occlusion) * visibility * att;
//    if(has_bones)
//        gl_FragColor = bone_color;
}
