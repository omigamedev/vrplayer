uniform mat4 proj;
uniform mat4 modelview;

void main()
{
    gl_Position = proj * modelview * gl_Vertex;
}
