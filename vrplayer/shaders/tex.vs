#version 120
uniform mat4 proj;
uniform mat4 view;
uniform mat4 lproj;
uniform mat4 lview;
uniform mat3 nview;
uniform ivec4 vp;
uniform vec3 cam_pos;
varying vec2 texcoord;
varying vec3 norm;
varying vec3 pos;
varying vec2 screen;
varying float depth;
varying float cam_att;

void main()
{
    gl_Position = proj * view * gl_ModelViewMatrix * gl_Vertex;
    texcoord = gl_MultiTexCoord0.st;
    norm = gl_NormalMatrix * gl_Normal;
    pos = vec3(gl_ModelViewMatrix * gl_Vertex);
    
    vec4 lpos = lproj * lview * gl_ModelViewMatrix * gl_Vertex;
    screen = (lpos.xy / lpos.w);
    depth = lpos.z * 0.001;
    
    vec3 camdiff = pos-cam_pos;
    float dist = dot(camdiff, camdiff);
    cam_att = clamp(1000/dist, 0, 1);
}
