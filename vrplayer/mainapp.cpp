#include "mainapp.h"

#ifdef MP4_VIDEO
    TheoraOutputMode outputMode = TH_BGRX;
    unsigned int textureFormat  = GL_BGRA_EXT;
    unsigned int uploadFormat   = GL_BGRA_EXT;
#else
    TheoraOutputMode outputMode = TH_RGB;
    unsigned int textureFormat  = GL_RGB;
    unsigned int uploadFormat   = GL_RGB;
#endif

omiMainApp::omiMainApp()
{
    m_stereo = true;
}

bool omiMainApp::init()
{
    printf("Vendor: %s\n", glGetString(GL_VENDOR));
    printf("Renderer: %s\n", glGetString(GL_RENDERER));
    printf("GL Version: %s\n", glGetString(GL_VERSION));
    printf("GLSH Version: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));

    riftInit();

    loadShader("color-flat");
    useShader("color-flat");
    uniformMatrix4f("proj", m_proj);
    uniform4f("color", vec4(1,1,0,1));
    
    loadShader("sky");
    useShader("sky");
    uniformMatrix4f("proj", m_proj);
    uniform1i("tex0", 0);
    
    loadShader("barrel");
    useShader("barrel");
    uniformMatrix4f("proj", mat4());
    uniform1i("tex0", 0);

    glViewport(0, 0, m_screen.x, m_screen.y);
    
    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEPTH);
    glDisable(GL_CULL_FACE);
    
    glEnableClientState(GL_VERTEX_ARRAY);
    glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    glEnableClientState(GL_NORMAL_ARRAY);
    
    m_rttSize = ivec2(1280, 800);
    createColorBuffer(m_rttSize, &m_rttTexture, &m_rttFBO);
    
    m_plane.create(2, 2);
    m_plane.shaded = false;
    m_plane.color = vec4(1,1,0,1);
    m_plane.material.diffuse_tex = m_rttTexture;
    
    loadOBJ("sphere", "sphere.obj", "/media/");
    rSphere = m_groups["sphere"].meshes[0];
    rSphere.model = rotate(90.0f, 0.0f, 1.0f, 0.0f);
//    rSphere.texmat = scale(1.0f, -1.0f, 1.0f);
    
    m_imageR = loadTexture("/media/cabin.0010_0001R.png");
    m_imageL = loadTexture("/media/cabin.0010_0001L.png");
//    rSphere.material.diffuse_tex = loadTexture("/media/panorama.png", false);

//    char str[256];
//    for(int i = 0; i < 300; i++)
//    {
//        sprintf(str, "media/Left/Left_%05d.jpg", i);
//        m_imageL = loadTexture(str);
//        sprintf(str, "media/Right/Right_%05d.jpg", i);
//        m_imageR = loadTexture(str);
//    }
    
    m_theoraManager = new TheoraVideoManager(2);
    m_theoraManager->setDefaultNumPrecachedFrames(128);
    m_theoraClipL = m_theoraManager->createVideoClip(m_resourcePath + "/media/rc_complete_left.ogv");
    m_theoraClipR = m_theoraManager->createVideoClip(m_resourcePath + "/media/rc_complete_right.ogv");
    m_theoraClipL->setAutoRestart(true);
    m_theoraClipR->setAutoRestart(true);
    int w = nextPow2(m_theoraClipL->getWidth());
    int h = nextPow2(m_theoraClipL->getHeight());
    m_imageL = createTexture(w, h, textureFormat);
    m_imageR = createTexture(w, h, textureFormat);
    m_theoraClipL->waitForCache();
    m_theoraClipR->waitForCache();
    
    float ratioU = 1.0f * ((float)m_theoraClipL->getWidth() / (float)w);
    float ratioV = 1.0f * ((float)m_theoraClipL->getHeight() / (float)h);
    rSphere.texmat = translate(0.0f, -(1-ratioV), 0.0f) * scale(ratioU, -ratioV, 1.0f);
    
    timeStart = getTime();
    printf("ready\n");
    return true;
}

map<TheoraVideoClip*, int> lastFrame;
void omiMainApp::drawVideo(GLuint tex_id, TheoraVideoClip* clip)
{
    glBindTexture(GL_TEXTURE_2D,tex_id);
    TheoraVideoFrame* f = clip->getNextFrame();
    if(f)
    {
        if(lastFrame[clip] != f->getFrameNumber())
            glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, clip->getWidth(), f->getHeight(), uploadFormat, GL_UNSIGNED_BYTE, f->getBuffer());
        lastFrame[clip] = f->getFrameNumber();
        clip->popFrame();
    }
}

void omiMainApp::renderEye(StereoEye eye)
{
    DistortionConfig dist = stereo.GetDistortionConfig();
    StereoEyeParams sep = stereo.GetEyeRenderParams(eye);
    
    if(sep.Eye == StereoEye_Right)
        dist.XCenterOffset = -dist.XCenterOffset;
    
    Viewport VP = sep.VP;
    mat4 adjust = make_mat4(&sep.ViewAdjust.Transposed().M[0][0]);
    mat4 proj = make_mat4(&sep.Projection.Transposed().M[0][0]);
//    print4("proj", proj);
//    
    float fov = degrees(2.1968629f);
    float aspect = (float)m_screen.x/(float)m_screen.y*0.5f;
//    print4("glm", translate(vec3(0.15197642,0,0)) * perspective(fov, aspect, 0.01f, 1000.0f));
//    mat4 ovrMat = make_mat4(&Matrix4f::PerspectiveRH(DegreeToRad(85.0f), (float)m_screen.x/(float)m_screen.y, 0.01f, 1000.0f).Transposed().M[0][0]);
//    print4("ovrMat", ovrMat);
    
    float w = float(VP.w) / float(m_screen.x),
        h = float(VP.h) / float(m_screen.y),
        x = float(VP.x) / float(m_screen.x),
        y = float(VP.y) / float(m_screen.y);
    float as = m_screen.x / (2.0f * m_screen.y);
    float scaleFactor = 1.0f / dist.Scale;
    
    if(STEREO) glViewport(x * m_screen.x, 0, m_screen.x/2, m_screen.y);
    else glViewport(0, 0, m_screen.x, m_screen.y);

    glBindFramebuffer(GL_FRAMEBUFFER, m_rttFBO);
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    
    useShader("sky");
    uniformMatrix4f("proj", proj);
    uniformMatrix4f("modelview", riftOrient() * rotate(-m_rot.y, 1.0f, 0.0f, 0.0f) * rotate(m_rot.x-90.0f, 0.0f, 1.0f, 0.0f) * scale(vec3(10)));
    uniformMatrix4f("texmat", rSphere.texmat);

    if(sep.Eye == StereoEye_Left || sep.Eye == StereoEye_Center || !m_stereo)
        rSphere.material.diffuse_tex = m_imageR;
    else
        rSphere.material.diffuse_tex = m_imageL;
    
    rSphere.draw(GL_TRIANGLES);
    
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    
    useShader("barrel");
    uniformMatrix4f("proj", mat4());
    uniformMatrix4f("modelview", lookAt(vec3(0), vec3(0,0,-1), vec3(0,1,0)));
    uniform2f("LensCenter", vec2(x + (w + dist.XCenterOffset * 0.5f)*0.5f, y + h*0.5f));
    uniform2f("ScreenCenter", vec2(x + w*0.5f, y + h*0.5f));
    uniform2f("Scale", vec2((w/2) * scaleFactor, (h/2) * scaleFactor * as));
    uniform2f("ScaleIn", vec2((2/w), (2/h) / as));
    uniform4f("HmdWarpParam", vec4(dist.K[0], dist.K[1], dist.K[2], dist.K[3]));
    
    if(!STEREO)
    {
        useShader("sky");
        uniformMatrix4f("proj", mat4());
        uniformMatrix4f("modelview", lookAt(vec3(0), vec3(0,0,-1), vec3(0,1,0)));
    }
    
    glViewport(0, 0, m_screen.x, m_screen.y);
    if(STEREO) glEnable(GL_SCISSOR_TEST);
    if(STEREO) glScissor(x * m_screen.x, 0, 640, 800);

    glDisable(GL_DEPTH_TEST);
    m_plane.draw(GL_TRIANGLES);
    glEnable(GL_DEPTH_TEST);
    glBindTexture(GL_TEXTURE_2D, 0);
    glDisable(GL_SCISSOR_TEST);
    
}

void omiMainApp::draw()
{
    glClearColor(0, 1, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
    
    long t = getTime();
//    char str[256];
//    if(t - timeStart > 30)
//    {
//        sprintf(str, "media/render/camera_left/shaderscene_%04d.jpg", frame);
//        m_imageL = loadTexture(str, false);
//        sprintf(str, "media/render/camera_right/shaderscene_%04d.jpg", frame);
//        m_imageR = loadTexture(str, false);
//        frame = (frame+1) % 300 + 1;
//        timeStart = t;
//        printf("tick\n");
//    }
    
    m_theoraManager->update((t - timeStart) * 0.001f);
    timeStart = t;
    drawVideo(m_imageR, m_theoraClipR);
    drawVideo(m_imageL, m_theoraClipL);
    
    renderEye(STEREO ? StereoEye_Left : StereoEye_Center);
    if(STEREO)
        renderEye(StereoEye_Right);
    
    glFlush();
}

void omiMainApp::resize(int w, int h)
{
    m_screen = ivec2(w, h);
    glViewport(0, 0, w, h);
    stereo.SetFullViewport(Viewport(0, 0, w, h));
}

void omiMainApp::mouseDown(const ivec2 &pos, int button)
{
    switch (button)
    {
    case BUTTON_LEFT:
        break;
    case BUTTON_RIGHT:
        break;
    case BUTTON_MIDDLE:
        break;
    }
}

void omiMainApp::mouseUp(const ivec2 &pos, int button)
{
    
}

void omiMainApp::mouseMove(const ivec2 &pos)
{

}

void omiMainApp::mouseDrag(const ivec2 &pos, const ivec2 &start, const ivec2 &delta, int button)
{
    switch(button)
    {
    case BUTTON_LEFT:
        m_rot += vec2((float)delta.x, (float)delta.y);
        break;
    case BUTTON_RIGHT:
        break;
    case BUTTON_MIDDLE:
        break;
    }
}

void omiMainApp::mouseWheel(const ivec2 &pos, vec2 value)
{
}

GLuint omiMainApp::createTexture(int w,int h,unsigned int format)
{
    unsigned int tex_id;
    glGenTextures(1,&tex_id);
    glBindTexture(GL_TEXTURE_2D,tex_id);
    glTexImage2D(GL_TEXTURE_2D, 0, format == GL_RGB ? GL_RGB : GL_RGBA, w, h, 0, format, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    return tex_id;
}

int omiMainApp::nextPow2(int x)
{
    int y = 1;
    for(; y < x; y *= 2);
    return y;
}
