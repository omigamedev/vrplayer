#ifndef _OMI_APP_
#define _OMI_APP_

#ifdef WIN32
    #include <Windows.h>
    #include <GL\glew.h>
    #include <gl\GL.h>
    #include <sys/types.h>
    #include <sys/timeb.h>
#else
    #include <sys/time.h>
#endif

#include <OVR.h>
#include <glm/glm.hpp>
#include <stdlib.h>
#include <fstream>
#include "shader.h"
#include "primitive.h"
#include "tiny_obj_loader.h"
#include "SOIL.h"

#define STEREO true

#define BUTTON_LEFT     0
#define BUTTON_RIGHT    1
#define BUTTON_MIDDLE   2

using namespace OVR;
using namespace OVR::Util::Render;
using namespace std;
using namespace glm;
using namespace tinyobj;

class omiAppBase
{
public:
    static omiAppBase *s;
    string m_basePath;
    string m_resourcePath;
    ivec2 m_screen;
    mat4 m_proj, m_lproj;
    mat4 m_view, m_lview;
    vec3 m_lightPosition;
    bool m_keys[256];
    map<string, omiShader> m_shaders;
    map<string, GLuint> m_textures;
    map<string, omiGroup> m_groups;
    omiShader *m_currentShader;

    OVR::Util::Render::StereoConfig stereo;
    OVR::Ptr<OVR::DeviceManager>    pManager;
    OVR::Ptr<OVR::SensorDevice>     pSensor;
    OVR::Ptr<OVR::HMDDevice>        pHMD;
    OVR::Ptr<OVR::Profile>          pUserProfile;
    OVR::SensorFusion               SFusion;
    OVR::HMDInfo                    TheHMDInfo;

    omiAppBase();
    
    virtual ~omiAppBase() { s = NULL; };
    virtual bool init() = 0;
    virtual void draw() = 0;
    virtual void resize(int w, int h) = 0;
    virtual void keyDown(int keycode){ m_keys[keycode] = true; };
    virtual void keyUp(int keycode){ m_keys[keycode] = false; };
    virtual void mouseDown(const ivec2 &pos, int button){};
    virtual void mouseUp(const ivec2 &pos, int button){};
    virtual void mouseMove(const ivec2 &pos){};
    virtual void mouseDrag(const ivec2 &pos, const ivec2 &start,
                           const ivec2 &delta, int button){};
    virtual void mouseWheel(const ivec2 &pos, vec2 value){};
    
    void riftInit();
    mat4 riftOrient();
    long getTime();
    bool createDepthBuffer(const ivec2 &size, GLuint *outTexture, GLuint *outFBO);
    bool createColorBuffer(const ivec2 &size, GLuint *outTexture, GLuint *outFBO);
    void drawElement(omiElement &el, mat4 *transform = 0);
    void drawGroup(const omiGroup &el, mat4 *transform = 0);
    bool loadOBJ(const string &id, const string &name, const string &base);
    GLuint loadTexture(const string &name, bool mipmaps = false);
    
    bool loadShader(const string &name);
    void useShader(const string &name);
    void uniformMatrix4f(const string &name, const mat4 &mat);
    void uniformMatrix4fv(const string &name, const float *mats, int count);
    void uniformMatrix3f(const string &name, const mat3 &mat);
    void uniform1i(const string &name, GLuint n);
    void uniform2i(const string &name, const ivec2 &v);
    void uniform3i(const string &name, const ivec3 &v);
    void uniform4i(const string &name, const ivec4 &v);
    void uniform1f(const string &name, GLfloat f);
    void uniform2f(const string &name, const vec2 &v);
    void uniform3f(const string &name, const vec3 &v);
    void uniform4f(const string &name, const vec4 &v);
    
    char* readFile(const string &filename, long *length = 0);
    float randf();
    float randfn();
};

#endif
