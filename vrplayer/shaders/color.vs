#version 120
uniform mat4 proj;
uniform mat4 view;
varying vec4 color;
void main()
{
    gl_Position = proj * view * gl_ModelViewMatrix * gl_Vertex;
    color = gl_Color;
}
